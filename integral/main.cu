#include <math.h>
#include <stdio.h>
#include <cstdio>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <time.h>
#include <stdlib.h>

using namespace std;

#define N 100000000
#define div 10000

__global__ void f(int** akf, int numSize){
    //blockIdx
    //thredIdx
    //blockDim
    //long long idx = blockIdx.x * blockDim.x + threadIdx.x;
	int toDivide = 1 << threadIdx.x;
	short digit = (blockIdx.x / toDivide) % 2;
	
	for (int i = threadIdx.x - 1; i <= 0; i--){
		int toDivide2 = 1 << (threadIdx.x - i);
		short digit2 = (blockIdx.x / toDivide2) % 2;
		akf[blockIdx.x][threadIdx.x] += digit * digit2;
	}
    
}

int main() {
	int numSize = 8;
    int arr[64][8];
    for(int i = 0; i<64; i++)
    {
        for(int j = 0; j<8; j++)
        {
            arr[i][j] = 0;
        }
    }

	int** mas = new int*[64];
    for(int i = 0; i<64; i++)
    {
        mas[i] = &arr[i][0];
    }


    clock_t start_GPU = clock();

    dim3 threads = dim3(8);
   
	int numSizeDev;
    int*** masDev[64][8];
   
    cudaMalloc((void*)&numSize, sizeof(int));
    cudaMalloc((void**)&arr, (64*8)*sizeof(int**));

    cudaMemcpy(&numSizeDev, numSize, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(masDev, mas, (64*8)*sizeof(int**), cudaMemcpyHostToDevice);
	



    f <<<blocks, threads>>> (masDev, numSizeDev);
    cudaMemcpy(mas, masDev, (64*8)*sizeof(int**), cudaMemcpyDeviceToHost);
    for (int i = 0; i < 64; i++){
        for (int j = 0; j < 8; j++){
			printf("%d: %d ", i, mas[i][j]);
		}
		printf("\n");
    }
/*
    clock_t end_GPU = clock();


    clock_t start_CPU = clock();

    for (long long i = 0; i < N; i ++){
        sumCPU += data[i];
    }

    clock_t end_CPU = clock();

    double seconds_GPU = (double)(end_GPU - start_GPU) / CLOCKS_PER_SEC * 1000;
    double seconds_CPU = (double)(end_CPU - start_CPU) / CLOCKS_PER_SEC * 1000;

    printf("GPU sum %d, time: %f\n", sum, seconds_GPU);
    printf("CPU sum %d, time: %f\n", sumCPU, seconds_CPU);*/
    return 0;
}


