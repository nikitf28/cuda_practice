#include <math.h>
#include <stdio.h>
#include <cstdio>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <time.h>
#include <stdlib.h>

using namespace std;

#define N 100000000
#define div 10000

__global__ void f(short *data, long long *sums){
    //blockIdx
    //thredIdx
    //blockDim
    long long idx = blockIdx.x * blockDim.x + threadIdx.x;
    for (long long i = 0; i < div; i++){
        if (idx*div + i >= N){
            break;
        }
        sums[idx] += data[idx*div + i];
    }
}

int main() {
    short* data = new short[N];
    long long* sumsDev;
    long long sumCPU = 0;
    for (long long i = 0; i < N; i++){
        data[i] = rand() % 200;
    }

    short* dataDev;
    long long sum = 0;


    clock_t start_GPU = clock();

    dim3 threads = dim3(64);
    long long* sums = new long long[N/div + 1];
    dim3 blocks = dim3(N/div/threads.x + 1);

    cudaMalloc((void**)&dataDev, N*sizeof(short));
    cudaMalloc((void**)&sumsDev, (N/div+1)*sizeof(long long));

    cudaMemcpy(dataDev, data, N*sizeof(short), cudaMemcpyHostToDevice);



    f <<<blocks, threads>>> (dataDev, sumsDev);
    cudaMemcpy(sums, sumsDev, (N/div+1)*sizeof(long long), cudaMemcpyDeviceToHost);
    for (long long i = 0; i < N/div + 1; i++){
        sum += sums[i];
    }

    clock_t end_GPU = clock();


    clock_t start_CPU = clock();

    for (long long i = 0; i < N; i ++){
        sumCPU += data[i];
    }

    clock_t end_CPU = clock();

    double seconds_GPU = (double)(end_GPU - start_GPU) / CLOCKS_PER_SEC * 1000;
    double seconds_CPU = (double)(end_CPU - start_CPU) / CLOCKS_PER_SEC * 1000;

    printf("GPU sum %d, time: %f\n", sum, seconds_GPU);
    printf("CPU sum %d, time: %f\n", sumCPU, seconds_CPU);
    return 0;
}


